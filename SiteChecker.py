from time import sleep
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException


class SiteCheck:
    def __init__(self, browser="Chrome"):
        self.site_url = 'http://automationpractice.com/index.php'
        if browser == "Chrome":
            self.driver = webdriver.Chrome()
        elif browser == "Firefox":
            self.driver = webdriver.Firefox()
        else:
            print("Wrong browser name is chosen - please use Chrome or Firefox")

    def login_with_any_credentials(self, login='afaf@sgdfgd.fg', password='245345'):
        self.driver.get(self.site_url)
        sign_in_button = self.driver.find_element_by_class_name('login')
        sign_in_button.click()
        username = self.driver.find_element_by_id("email")
        password_element = self.driver.find_element_by_id("passwd")
        username.send_keys(login)
        password_element.send_keys(password)
        self.driver.find_element_by_id("SubmitLogin").click()
        # sleep - just in order to observe that actions were executed
        sleep(3)

    def logout_ui(self):
        # explicit waits also could be used here if used after log in
        try:
            self.driver.find_element_by_class_name('logout').click()
            # sleep - just in order to observe that actions were executed
            sleep(3)
        except NoSuchElementException:
            print("No user is registered with such credentials.")
            self.driver.close()

    def logout_by_url(self):
        self.driver.find_element_by_partial_link_text("Sign out").click()
        # or maybe it was just about
        # self.driver.get('http://automationpractice.com/index.php?mylogout=')

        # sleep - just in order to observe that actions were executed
        sleep(3)

    def close_browser(self):
        self.driver.close()


# Examples of Test cases
# Test case 1. Check log in with valid credentials for existing user.
# Logout using found logout button on UI.
A = SiteCheck("Chrome")
A.login_with_any_credentials()
A.logout_ui()
# A.close_browser()

# Test case 2. Check log in with valid credentials for existing user.
# Logout using found logout button on UI.

A.login_with_any_credentials()
A.logout_by_url()
A.close_browser()

# Test case 3. Check log in with not valid credentials for existing user.
# Logout using found logout button on UI does not work - no such button.
# It is handled as exception (but authentication error handling could be added also here).
# Firefox browser is used to confirm it's supported.

B = SiteCheck("Firefox")
B.login_with_any_credentials(login='afaf@sgdfgd4.fg', password='245345')
B.logout_ui()


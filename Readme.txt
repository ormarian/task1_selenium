How to run:
1. Create instanse of SiteCheck class stating the browser (Chrome or Firefox) which will be used.
2. Use functions defined in SiteChecker class.

Example:
A = SiteCheck("Chrome")
A.login_with_any_credentials()
A.logout_ui()
A.close_browser()
